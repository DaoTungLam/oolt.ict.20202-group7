package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import stock.Stock;
import stock.StockExchange;

public class StockTest {
	public static void main(String[] args) {
		StockExchange hose = new StockExchange("HOSE");
		
		try {
			File inputFile = new File("HOSE.txt");
			Scanner fileReader = new Scanner(inputFile);

			while (fileReader.hasNextLine()) {
				String buf = fileReader.nextLine();
                hose.addStock(new Stock(buf));
            }

            fileReader.close();
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
        }
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Ticker: ");
		String ticker = sc.next();
		if (hose.getStock(ticker) == null) {
			System.out.println("No Ticker " + ticker);
		} else {
			System.out.println("\n" + hose.getStock(ticker).toString());
		}
		
		sc.close();
	}
}
