package stock;

import java.text.DecimalFormat;

public class Stock {
	private String ticker;
	private double ceilingPrice;
	private double floorPrice;
	private double refPrice;
	private double closePrice;
	private int tradeVolume;
	private int ptVolume;
	private double highestPrice;
	private double lowestPrice;

	public Stock(String ticker, double ceilingPrice, double floorPrice, double refPrice, double closePrice,
			int tradeVolume, int ptVolume, double highestPrice, double lowestPrice) {
		this.ticker = ticker;
		this.ceilingPrice = ceilingPrice;
		this.floorPrice = floorPrice;
		this.refPrice = refPrice;
		this.closePrice = closePrice;
		this.tradeVolume = tradeVolume;
		this.ptVolume = ptVolume;
		this.highestPrice = highestPrice;
		this.lowestPrice = lowestPrice;
	}

	public Stock(String rawLine) {
		String[] list = rawLine.split("\\s");
		this.ticker = list[0];
		this.ceilingPrice = stringToNum(list[1]);
		this.floorPrice = stringToNum(list[2]);
		this.refPrice = stringToNum(list[3]);
		this.closePrice = stringToNum(list[4]);
		this.tradeVolume = (int) stringToNum(list[5]);
		this.ptVolume = (int) stringToNum(list[6]);
		this.highestPrice = stringToNum(list[7]);
		this.lowestPrice = stringToNum(list[8]);
	}
	
	private double stringToNum(String numString) {
		numString = numString.replace(",", "");
		if (numString.matches("[+-]?(\\d*[.])?\\d+") == false) {
			throw new NumberFormatException("Invalid Number " + numString);
		}
		
		return Double.parseDouble(numString);
	}
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#,###");
		
		String s = "Ticker: " + ticker + "\n";
		s += "Ceiling Price: " + ceilingPrice + "\n";
		s += "Floor Price: " + floorPrice + "\n";
		s += "Reference Price: " + refPrice + "\n";
		s += "Closing Price: " + closePrice + "\n";
		s += "Trade Volume: " + df.format(tradeVolume) + "\n";
		s += "Put Through Volume: " + df.format(ptVolume) + "\n";
		s += "Highest Price: " + highestPrice + "\n";
		s += "Lowest Price: " + lowestPrice;
		return s;
	}
	
	public double getDifference() {
		return closePrice - refPrice;
	}
	
	public double getPercentage() {
		return getDifference() * 100 / refPrice;
	}

	public String getTicker() {
		return ticker;
	}

	public double getCeilingPrice() {
		return ceilingPrice;
	}

	public double getFloorPrice() {
		return floorPrice;
	}

	public double getRefPrice() {
		return refPrice;
	}

	public double getClosePrice() {
		return closePrice;
	}

	public int getTradeVolume() {
		return tradeVolume;
	}

	public int getPtVolume() {
		return ptVolume;
	}

	public double getHighestPrice() {
		return highestPrice;
	}

	public double getLowestPrice() {
		return lowestPrice;
	}
}
