package stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StockExchange {
	private String name;
	private List<Stock> stockList = new ArrayList<>();
	
	public StockExchange(String name) {
		this.name = name;
	}
	
	public Stock getStock(String ticker) {
		for (Stock stock: this.stockList) {
			if (stock.getTicker().equals(ticker)) {
				return stock;
			}
		}
		
		return null;
	}
	
	public void addStock(Stock stock) {
		if (getStock(stock.getTicker()) != null) {
			return;
		}
	
		this.stockList.add(stock);
	}

	public Stock getMax(Comparator<Stock> c) {
		Stock maxStock = stockList.get(0);
		for (Stock stock: stockList) {
			if (c.compare(maxStock, stock) < 0) {
				maxStock = stock;
			}
		}

		return maxStock;
	}

	public Stock getMin(Comparator<Stock> c) {
		Stock minStock = stockList.get(0);
		for (Stock stock: stockList) {
			if (c.compare(minStock, stock) > 0) {
				minStock = stock;
			}
		}

		return minStock;
	}

	public Stock[] getNLargest(int n, Comparator<Stock> c) {
		Stock[] stockArr = stockList.toArray(new Stock[0]);

		if (stockArr.length < n) {
			return null;
		}

		for (int i = 0; i < n; i++) {
			for (int j = stockArr.length - 1; j > i; j--) {
				if (c.compare(stockArr[j - 1], stockArr[j]) < 0) {
					swap(stockArr, j, j - 1);
				}
			}
		}

		return Arrays.copyOf(stockArr, n);
	}

	public Stock[] getNSmallest(int n, Comparator<Stock> c) {
		Stock[] stockArr = stockList.toArray(new Stock[0]);

		if (stockArr.length < n) {
			return null;
		}

		for (int i = 0; i < n; i++) {
			for (int j = stockArr.length - 1; j > i; j--) {
				if (c.compare(stockArr[j - 1], stockArr[j]) > 0) {
					swap(stockArr, j, j - 1);
				}
			}
		}

		return Arrays.copyOf(stockArr, n);
	}

	private void swap(Stock[] stockArr, int i, int j) {
		Stock tmp = stockArr[i];
		stockArr[i] = stockArr[j];
		stockArr[j] = tmp;
	}

	public String getName() {
		return name;
	}

	public List<Stock> getStockList() {
		return Collections.unmodifiableList(stockList);
	}
}
