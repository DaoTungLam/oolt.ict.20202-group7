package tag;

import java.util.ArrayList;
import java.util.List;

public class Tag implements Comparable<Tag> {
    private String name;
    private List<String> wordList = new ArrayList<>();

    public Tag(String rawTextLine) {
        String[] words = rawTextLine.split(",");

        name = words[0].trim();
        for (String s: words) {
            wordList.add(s.trim());
        }
    }

    public boolean isIn(String sentence) {
        for (String word: wordList) {
            if (sentence.toLowerCase().contains(word.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    public boolean hasWord(String word) {
        for (String s: wordList) {
            if (s.equalsIgnoreCase(word)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int compareTo(Tag tag) {
        return name.compareToIgnoreCase(tag.name);
    }

    public String getName() {
        return name;
    }
}
