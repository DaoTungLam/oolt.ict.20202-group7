package tag;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public final class TagList {
    private static final List<Tag> tagList = new ArrayList<>();

    static {
        readFile("tag_words.txt"); // Assign tagList content
        Collections.sort(tagList); // Sort to use binary search
    }

    public static void readFile(String fileName) {
        try {
            File tagFile = new File(fileName);
            Scanner fileReader = new Scanner(tagFile);

            tagList.clear();
            while (fileReader.hasNextLine()) {
                String buf = fileReader.nextLine();
                if (buf.equals("")) {
                    continue;
                }
                tagList.add(new Tag(buf));
            }

            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Tag searchByName(String name) {
        int left = 0, right = tagList.size();

        while (left < right) {
            int mid = (left + right) / 2;
            String midName = tagList.get(mid).getName();
            
            int compare = midName.compareToIgnoreCase(name);
            if (compare == 0) {
                return tagList.get(mid);
            } else if (compare < 0) {
                mid = right - 1;
            } else {
                mid = left + 1;
            }
        }
        
        return null;
    }

    public static Tag searchByWord(String word) {
        for (Tag tag: tagList) {
            if (tag.hasWord(word)) {
                return tag;
            }
        }

        return null;
    }
}
